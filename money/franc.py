from money.money import Money


class Franc(Money):

    def times(self, multiplier):
        return Franc(multiplier * self._amount)
