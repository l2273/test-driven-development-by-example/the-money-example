from money.franc import Franc


def test_multiply_by_two():
    five = Franc(5)

    assert Franc(10) == five.times(2)


def test_multiply_same_dollar_twice():
    five = Franc(5)

    assert Franc(10) == five.times(2)
    assert Franc(15) == five.times(3)
