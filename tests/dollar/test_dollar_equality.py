from money.dollar import Dollar


def test_given_two_five_dollars_should_be_equal():
    assert Dollar(5) == Dollar(5)


def test_given_five_and_six_dollars_should_not_be_equal():
    assert Dollar(5) != Dollar(6)
