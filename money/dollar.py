from money.money import Money


class Dollar(Money):

    def times(self, multiplier):
        return Dollar(multiplier * self._amount)
