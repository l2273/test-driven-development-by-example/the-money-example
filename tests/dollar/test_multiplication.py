from money.dollar import Dollar


def test_multiply_by_two():
    five = Dollar(5)

    assert Dollar(10) == five.times(2)


def test_multiply_same_dollar_twice():
    five = Dollar(5)

    assert Dollar(10) == five.times(2)
    assert Dollar(15) == five.times(3)
