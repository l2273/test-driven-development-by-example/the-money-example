from dataclasses import dataclass


@dataclass(eq=False)
class Money:
    _amount: int

    def __eq__(self, other):
        return self._amount == other._amount and type(self) == type(other)
