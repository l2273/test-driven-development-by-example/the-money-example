from money.dollar import Dollar
from money.franc import Franc


def test_given_two_five_francs_should_be_equal():
    assert Franc(5) == Franc(5)


def test_given_five_and_six_francs_should_not_be_equal():
    assert Franc(5) != Franc(6)


def test_given_dollars_and_francs_should_not_be_equal():
    assert Franc(5) != Dollar(5)
