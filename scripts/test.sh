#!/bin/sh

rm -rf .env \
  && rm -rf tests/memory.sqlite \
  && python -m pytest tests/unit_tests -vv --capture=tee-sys \
  && rm -rf tests/memory.sqlite