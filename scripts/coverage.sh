#!/bin/sh

coverage run --source . -m pytest \
  && coverage report --omit=tests/*.py,setup.py,.venv/* \
  && coverage xml -o coverage-reports/coverage.xml --omit=tests/*.py,setup.py,venv/*,.venv/* \
  && coverage html -d coverage-reports/html --omit=tests/*.py,setup.py,venv/*,.venv/*